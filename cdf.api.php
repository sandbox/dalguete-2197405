<?php

/**
 * @file
 * API documentation for the Custom Display Fields module.
 * 
 * Hooks are the same as the ones used for Fields API, with some changes used to
 * treat this fields as display only:
 * 
 * -> hook_field_info(). A couple of new entries are handled here, in order to identify
 *    these fields as CDF ones:
 * 
 *    > 'cdf' entry. A flag used to tell cdf module to handle this field definition
 *    > 'entity type' entry. Name of the entity type this field can be attached to,
 *      if defined. If not, all entity types are supported, if and only if other
 *      criteria is satisfied
 *    > 'bundle' entry. Name of the bundle this field can be attached to, if defined. 
 *      If not, all bundles are supported, if and only if other criteria is satisfied
 *    > 'view mode' entry. Name of the view mode entity type this field can be attached 
 *      to, if defined. If not, all view modes are supported, if and only if other 
 *      criteria is satisfied
 * 
 * -> hook_field_formatter_info(). A couple of new entries are handled here, in order 
 *    to identify these field formatters as CDF ones:
 * 
 *    > 'cdf' entry. A flag used to tell cdf module to handle this field formatter 
 *      definition. Also imposes an explicit condition that indicates this formatter
 *      can only be used in CDF fields, despite what says in 'field types'
 *    > 'entity type' entry. Name of the entity type this field formatter can be 
 *      used in, if defined. If not, all entity types are supported, if and only 
 *      if other criteria is satisfied
 *    > 'bundle' entry. Name of the bundle this field formatter can be attached to, 
 *      if defined. If not, all bundles are supported, if and only if other criteria 
 *      is satisfied
 *    > 'view mode' entry. Name of the view mode entity type this field formatter 
 *      can be attached to, if defined. If not, all view modes are supported, if 
 *      and only if other criteria is satisfied
 * 
 * TODO: in the future support negative cases, and complex cases, like the next:
 *  - negative: display in all entity types that are not a 'Node'
 *  - complex: display in all nodes with bundle article or in all not Users. All of
 *    these in the same definition
 * 
 * -> All info set in CDF related fields and formatters will be altered, so they 
 *    can not be added via UI or have widgets, so don't worry about setting no_ui 
 *    in hook_field_info or implement blank widgets, this module handles that for you.
 * 
 * -> Despite the fact fields and formatters are defined in your module, these are 
 *    treated as owned by CDF, so all defining field type hooks and field formatter 
 *    type hooks are handled by CDF, where redirection happens to your implementations 
 *    of those hooks. For instance, you can implement hook_field_formatter_view() 
 *    and hope it works as usual, but because CDF module hijacks it and rethrows 
 *    it back at you. 
 *    In case of fields, this is done this way to ensure the field is effectively 
 *    display only, and hooks like hook_field_widget_form() are never called
 * 
 * -> Due to point above, there's no need to define a default_formatter in field
 *    definition, unless you want to use your formatter or another module's formatter,
 *    in which case you need to implement hook_field_formatter_info(). If you want
 *    to use the formatter provided by the module, check for 'cdf_formatter' in
 *    all formatter related hooks (like hook_field_formatter_view among others),
 *    to tackle it correctly
 * 
 * -> All CDF fields handle a custom storage set in the module, that prevents them
 *    to have data, thing that makes sense as those are display only
 * 
 * -> So, all display related Fields API hooks are available, no new hooks are 
 *    defined here. As you saw, just a new entries in hook_field_info() and hook_field_formatter_info() 
 *    and we are done.
 * 
 * -> All supported defining field type module hooks are:
 * 
 *    > hook_field_is_empty
 *    > hook_field_prepare_view
 *    > hook_field_prepare_translation
 *    > hook_field_formatter_prepare_view
 *    > hook_field_formatter_view
 *    > hook_field_formatter_settings_form
 *    > hook_field_formatter_settings_summary
 */
