/*
 * Behavior to control some stuff in the controllers selection
 */

(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.cdf_editarea = {
  attach: function(context, settings) {
    
    // Add a control for when the CDF text format is selected
    var $select = $('.filter-guidelines', context)
    .once('cdf-filter-guidelines', function(){
      // Load the lib
      Drupal.behaviors.cdf_editarea.loader(settings.cdf_editarea.hack_path, settings.cdf_editarea.lib_path);
    })
    .closest('.filter-wrapper')
    .find('select.filter-list')
    .bind('change', function () {
      // Get the textarea element
      var $textarea = $(this).closest('.text-format-wrapper').find('textarea').first();
      
      // Act only in the correct text format
      if(settings.cdf_editarea.filter_key !== this.value){
        // Destroy any editarea, if exists
        if ($textarea.hasClass('cdf-editarea-processed')) {
          editAreaLoader.delete_instance($textarea.attr('id'));
        }
        
        return;
      }
      
      // Explicitely call to window_loaded(), to indicate that object, all is ready
      // Part of the hack stuff
      editAreaLoader.window_loaded();
      
      // If possible, enable the editarea features
      editAreaLoader.init({
        id: $textarea.attr('id'),
        syntax: "php",
        start_highlight: true,
        allow_resize: "y",
        min_height: 250,
        allow_toggle: false,
        toolbar: "*",
        word_wrap: false,
        language: "en",
        replace_tab_by_spaces: 2,
        EA_init_callback: 'Drupal.behaviors.cdf_editarea.EA_init_callback',
        EA_delete_callback: 'Drupal.behaviors.cdf_editarea.EA_delete_callback',
        change_callback: 'Drupal.behaviors.cdf_editarea.change_callback'
      });
    });
    
    // In case the current selected text format is the one handled here, give it
    // some time before triggering the change event, where all the magic happens
    var func = function(){
      if (typeof editAreaLoader !== 'undefined'){
        $select.change();
        
        return;
      }
      
      setTimeout(func, 500);
    };
    
    if(settings.cdf_editarea.filter_key === $select.val()){
      setTimeout(func, 500);
    }
  },
  
  // EditArea lib loader
  loader: function(hack_path, lib_path) {
    // Init the EditArea object. Necessary as it is referenced in the script loaded below
    EditArea = function(){};

    // This is part of the hack. A file with the name "edit_area_" must exist, so 
    // the following call to EditArea lib can find the correct path to the source
    // lib dir. The file selected is edit_area_functions.js (check the module), as 
    // it doesn't do anything harmful
    //
    // For some reason, the loading via jQuery does not work, so the native DOM is
    // used in this case
    // 
    // http://stackoverflow.com/questions/610995/cant-append-script-element
    var hack_script = document.createElement("script");
    hack_script.type = "text/javascript";
    hack_script.src = "/" + hack_path;
    $('head', document)[0].appendChild(hack_script);

    // Load the EditArea lib
    var lib_script = document.createElement("script");
    lib_script.type = "text/javascript";
    lib_script.src = "/" + lib_path;
    $('head', document)[0].appendChild(lib_script);    
  },

  // Perform some work on the recently created EditorArea  
  EA_init_callback: function(id) {
    var $textarea = $("#" + id);
    
    // Mark the textarea as EditArea processed
    $textarea.addClass('cdf-editarea-processed');
    
    // As the 'change_callback' offered by EditArea is not bullet proof, we set
    // a handler for when the Update button is clicked, so the textarea can hold 
    // the last entered value. Also, in this process we ensure our handler is executed
    // first
    var $submit = $textarea.closest('.text-format-wrapper').next('.form-actions').find(':submit').first();
    
    // Ensure it's processed once
    $submit.once('cdf-editarea', function(){    
      var mousedowns = $submit.data("events").mousedown.slice();

      $submit.unbind("mousedown").mousedown(function() {
        Drupal.behaviors.cdf_editarea.change_callback(id);
      });

      $.each(mousedowns, function(i, v) {
        $submit.mousedown(v);
      });      
    });
  },

  // Perform some clean up on the recently deleted EditorArea
  EA_delete_callback: function(id) {
    var $textarea = $("#" + id);
    
    // Remove the mark added to EditArea processed
    $textarea.removeClass('cdf-editarea-processed');

    // Remove any handler set in the Update button
    var $submit = $textarea.closest('.text-format-wrapper').next('.form-actions').find(':submit').first();

    // Ensure it has been processed
    if ($submit.hasClass('cdf-editarea-processed')) {
      var mousedowns = $submit.data("events").mousedown.slice(1);
      
      $submit.unbind("mousedown");

      $.each(mousedowns, function(i, v) {
        $submit.mousedown(v);
      });      
      
      $submit.removeClass('cdf-editarea-processed');
    }    
  },
  
  // Update textarea with the EditArea code
  change_callback: function(id) {
    $('#' + id).val(editAreaLoader.getValue(id));
  }  
};

})(jQuery, Drupal, this, this.document);